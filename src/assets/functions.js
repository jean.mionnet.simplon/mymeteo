export const unixToHourAndMinutes = (unixInput) => {
    var sunHour = new Date(unixInput * 1000);
    // Hours part from the timestamp
    var hours = sunHour.getHours();
    // Minutes part from the timestamp
    var minutes = "0" + sunHour.getMinutes();
    
    // Will display time in 10:30:23 format
    var formattedTime = hours + 'h' + minutes.substr(-2);
    
    return formattedTime;
}