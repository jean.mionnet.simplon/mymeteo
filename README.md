# MyMeteo
## Installation du projet
```
npm install
```

### Lancer serveur de dévelopemment
```
npm run serve
```

### Compile et minifie pour la production
```
npm run build
```

### Lancer Lint 
```
npm run lint
```
